# shakmaty-uci

A Rust library to parse and write Universal Chess Interface (UCI) messages.

## Contributing

This library is [Free Software](LICENSES/GPL-3.0-or-later.txt) and every
contributions are welcome.

Please note that contributing to this project is subject to a [Contributor Code
of Conduct](CODE_OF_CONDUCT.md). By participating in this project you agree to
abide by its terms.
