//! A library to parse and write Universal Chess Interface (UCI) message.
//!
//! # Examples
//!
//! Ask the engine to evaluate a position:
//!
//! ```
//! use shakmaty_uci::UciMessage;
//!
//! println!("{}", UciMessage::Uci); // uci
//! assert_eq!("uciok".parse(), Ok(UciMessage::UciOk));
//! println!(
//!     "{}",
//!     UciMessage::Position{
//!         startpos: true,
//!         fen: None,
//!         moves: vec![
//!             "e2e4".parse().unwrap(),
//!             "e7e5".parse().unwrap(),
//!             "g8f3".parse().unwrap(),
//!         ],
//!     }
//! );
//! println!("{}", UciMessage::go_infinite())
//! ```
//!

#![no_std]
#![forbid(unsafe_code)]
#![deny(rust_2018_idioms, nonstandard_style)]
#![warn(future_incompatible)]
#![warn(missing_debug_implementations)]
#![cfg_attr(docsrs, feature(doc_auto_cfg))]

#[macro_use]
extern crate alloc;

#[cfg(feature = "std")]
extern crate std;

#[allow(unused_imports)]
#[doc(no_inline)]
pub use shakmaty::uci::{IllegalUciMoveError, ParseUciMoveError, UciMove};

mod message;
mod parser;

#[cfg(test)]
mod tests;

pub use message::*;
